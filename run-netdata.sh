#!/bin/bash

balena-engine run -d \
  --name=netdata \
  --hostname=netdata \
  -v $(pwd)/netdataconfig:/etc/netdata \
  -v $(pwd)/netdatalib:/var/lib/netdata \
  -v $(pwd)/netdatacache:/var/cache/netdata \
  -v /etc/passwd:/host/etc/passwd:ro \
  -v /etc/group:/host/etc/group:ro \
  -v /proc:/host/proc:ro \
  -v /sys:/host/sys:ro \
  -v /etc/os-release:/host/etc/os-release:ro \
  -p 19999:19999 \
  --restart unless-stopped \
  --cap-add SYS_PTRACE \
  --security-opt apparmor=unconfined \
  --label 'traefik.enable=true' \
  --label 'traefik.http.routers.netdata.rule=Host(`netdata.ideascube.io`)' \
  --label 'traefik.http.services.netdata.loadbalancer.server.port=19999' \
  --network=olipcore \
  netdata/netdata
