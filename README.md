# netdata

`netdata` monitoring for the OLIP stack.

:warning: this setup is intended for manual installation.

It does NOT belong to the OLIP Apps catalog nor the regular OLIP stack.


## Usage

```shell
git pull && ./run.sh
```

The Netdata Dashboard is available at <http://netdata.ideascube.io/>.

However, in order to not add more load to the Traefik reverse proxy,
the dashboard is also available directly: http://ideascube.io:19999>.
